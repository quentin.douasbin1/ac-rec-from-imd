"""
This script reconstructs the acoustic variables
at a given locations using time varying amplitudes
"""

import numpy as np
import antares as ant

import ac_var as av
import amplitudes as amp
import export as exprt
import get_probes as pr
from inputParams import *

# -----------------
#    Input files
# -----------------

tp, a1, a2, a3 = amp.get_complex_amp(begin_read, end_read, inComplexAmpFile)

# Normalize the a_i

a1 /= norm_phi1
a2 /= norm_phi2
a3 /= norm_phi3

# We want detailed information from Antares
ant.verbose(4)

# ------------------
# Reading the mesh
# ------------------
r = ant.Reader("hdf_avbp")
r['filename'] = avspMeshPath
r['file_format'] = 'hdf_avbp'
r['shared'] = True
base_mesh = r.read()
base_mesh2 = r.read()
base_mesh3 = r.read()

# ------------------
# Reading the files
# ------------------
reader = ant.Reader("hdf_avbp")
reader['base'] = base_mesh
reader['filename'] = avspSol1
# reader['file_format'] = 'hdf_avbp'
r['shared'] = False
base_1T = reader.read()

read_2 = ant.Reader("hdf_avbp")
read_2['base'] = base_mesh2
read_2['filename'] = avspSol2
# read_2['file_format'] = 'hdf_avbp'
r['shared'] = False
base_1T_prime = read_2.read()

read_3 = ant.Reader("hdf_avbp")
read_3['base'] = base_mesh3
read_3['filename'] = avspSol3
# read_3['file_format'] = 'hdf_avbp'
r['shared'] = False
base_1R = read_3.read()

print base_1T_prime
print base_1T_prime[0]
print base_1T_prime[0][0]
print 'base_1t_prime.keys()', base_1T_prime.keys()

print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
print ' Done reading'
print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'

# -----------------------------
# Retrieve the probes family
# -----------------------------
# Reading the probe location from file using the function get_probes
treatment = ant.Treatment('probe')
treatment['base'] = base_1T
treatment['points'] = pr.get_probes()
probes_loc = treatment.execute()

# Add the family to the base
base_1T.families['my_probes'] = probes_loc
base_1T_prime.families['my_probes'] = probes_loc
base_1R.families['my_probes'] = probes_loc

# Extract the data at the probes locations
data_1T = base_1T[probes_loc]
data_1T_prime = base_1T_prime[probes_loc]
data_1R = base_1R[probes_loc]

# Retrieving Acoustic Variables at probe location
phi_1T = []
phi_1T_prime = []
phi_1R = []

for zone in data_1T.keys():
    phi_1T.append(av.get_complex_ac_var(data_1T[zone]))
    phi_1T_prime.append(av.get_complex_ac_var(data_1T_prime[zone]))
    phi_1R.append(av.get_complex_ac_var(data_1R[zone]))

# Adding a third dimension to multiply the arrays by the complex amplitudes
phi_1T = np.atleast_3d(phi_1T)
phi_1T_prime = np.atleast_3d(phi_1T_prime)
phi_1R = np.atleast_3d(phi_1R)

print
print 'shape phi_1T', np.shape(phi_1T)
print 'shape a1', np.shape(a1)
print

# -----------------------------
# Computing 1D data --> Temporal loop
# -----------------------------

# RECONSTRUCTION !
rcst = np.real(a1 * phi_1T + a2 * phi_1T_prime + a3 * phi_1R)

pb_nm = pr.get_probes_names()

exprt.export_1D_data_real_part(tp, data_1T, '../OUTPUT/' + outputRecFolder, rcst, 't p u v w', begin_read, end_read)

print
print ' ########## Output files written ########## '
print

print 'getting complex basis'

cphi_1T, cphi_1T_prime, cphi_1R = av.get_ac_var(data_1T, data_1T_prime, data_1R)

print
print
print

exprt.export_basis(data_1T, data_1T_prime, data_1R, cphi_1T, cphi_1T_prime, cphi_1R)

print 'End of the program'
print
