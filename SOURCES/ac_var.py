__author__ = 'qdouasbi'

import numpy as np
import cmath as c

def get_complex_ac_var(bse):

    p_hat = np.array(c.rect(bse[0]['abspress'], bse[0]['argpress']))
    u_hat =-np.array(c.rect(bse[0]['absspeed_x'], bse[0]['argspeed_x']))
    v_hat =-np.array(c.rect(bse[0]['absspeed_y'], bse[0]['argspeed_y']))
    w_hat =-np.array(c.rect(bse[0]['absspeed_z'], bse[0]['argspeed_z']))

    #print p_hat

    return p_hat, u_hat, v_hat, w_hat

def get_ac_var(data_1T, data_1T_prime, data_1R):
	cphi_1T = []
	cphi_1T_prime = []
	cphi_1R = []

	for zone in data_1T.keys():
	    cphi_1T.append(get_complex_ac_var(data_1T[zone]))
	    cphi_1T_prime.append(get_complex_ac_var(data_1T_prime[zone]))
	    cphi_1R.append(get_complex_ac_var(data_1R[zone]))

	return cphi_1T, cphi_1T_prime, cphi_1R
