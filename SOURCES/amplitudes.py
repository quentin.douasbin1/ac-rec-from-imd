__author__ = 'qdouasbi'

import numpy as np
import matplotlib.pyplot as pl

def get_complex_amp(begin, end, path):
    """
    Loads the complex amplitudes
    :return: 4*1D array. 1= temporal; (2, 3, 4) = amplitudes of (phi1, phi2, phi3)
    """
    print 'Loading Data...'

    #Complex data from non-normalized modal projection using AVSP basis
    data = np.loadtxt(path, dtype=np.complex128)

    tp = data.real[begin:end, 0]
    a1 = data[begin:end, 1]
    a2 = data[begin:end, 2]
    a3 = data[begin:end, 3]

    print'Data Loaded'

    return tp, a1, a2, a3

