import numpy as np
import get_probes as pr

def read_data(name, path, begin, end, skp):
    out = []
    for filename in name:
	data = np.loadtxt(path + filename, skiprows=skp)
	print 'Loading', filename

	p_t_pr = data[begin:end, 1]  # p(t) at probe
	out.append(p_t_pr)

    temporal = data[begin:end, 0]  # time vector  # Assuming it is the same everywhere

    print

    out = np.array(out) - np.mean(out)

    return np.transpose(out), temporal


########def get_data_simulation(begin, end, path):
########    """
########    Function to load the simulation data
########    :return: 2D Array of pressure*probes , 1D array of time
########    """
########    name = ['p_C1.dat', 'p_C2.dat', 'p_C3.dat', 'p_C4.dat', 'p_C5.dat', 'p_C6.dat',
########	    'p_C7.dat', 'p_C8.dat', 'p_C9.dat', 'p_C10.dat', 'p_C11.dat', 'p_C12.dat']

########    return read_data(name, path, begin, end, 0)



def get_data_all_probes_avbp(begin, end, path):
    """
    Function to load the simulation data
    :return: 2D Array of pressure*probes , 1D array of time
    """
    name = []
    pb_nm  = pr.get_probes_names() 

    for nm in pb_nm:
    	name.append('avbp_local_' + nm + '.dat')

    return read_data(name, path, begin, end, 1)
