import numpy as np
#import time as time

def get_probes():
    """
    Loads the probes location data

    """
    print 'Loading probes...'

    # data from non-normalized modal projection using AVSP basis
    # data = np.loadtxt('./probes/probes')
    #data = np.loadtxt('../INPUTS/PROBES/probes_BKD')
    data = np.loadtxt('../INPUTS/PROBES/probes_test_cases_RingC.dat')

    x = data[:, 0]
    y = data[:, 1]
    z = data[:, 2]

    probes = []
    for i, xx in enumerate(x):
        probes.append((x[i], y[i], z[i]))

    print 'probes loaded'

    return probes

def get_probes_names():
    """
    Loads the probes location data

    """
    print 'Loading probes names...'

    myfile = '../INPUTS/PROBES/probes_test_cases_RingC_names.dat'

    FH = open(myfile, "r")
    out = [ i.rstrip() for i in FH.readlines()]
    print 'probes names loaded'

    return out
