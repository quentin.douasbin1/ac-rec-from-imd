"Input file for the Complex Reconstruction"

# NORMS
# Hermitian norms form Complex IMD of mode 1, 2 and 3
norm_phi1 = 0.118707
norm_phi2 = 0.116709
norm_phi3 = 0.990182

# Reading input files option : (0, -1) = from first line to last
begin_read = 0
end_read = -1

# Path of the complex amplitudes files
inComplexAmpFile = '../INPUTS/AMP/TEST_CASES/Complex_Inst_Amplitudes_Test_Case_AMT_1T_10Pa_no_norm.ascii'

# Path of the AVSP MESH
avspMeshPath = '../INPUTS/MODES/AVSP/MESH/TEST_CASE_AC_REC_AVSP.mesh.h5'

# Path of the AVSP SOLUTIONS 
avspSol1 = '../INPUTS/MODES/AVSP/TEST_CASES/computed_solution_0000005.h5'
avspSol2 = '../INPUTS/MODES/AVSP/TEST_CASES/computed_solution_0000006.h5'
avspSol3 = '../INPUTS/MODES/AVSP/TEST_CASES/computed_solution_0000033.h5'

# Name of the Output folder
outputRecFolder = 'Test_Case_AMT_1T_10_Pa'
